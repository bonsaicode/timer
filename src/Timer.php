<?php
namespace BonsaiCode;
/**
 * Timer class
 *
 * Time/benchmark your code.  Based on https://davidwalsh.name/php-timer-benchmark
 *
 * @author Alan Davis <alan@bonsaicode.dev>
 * @copyright 2016 BonsaiCode, LLC
 *
 */

class Timer {

	/**
	 *
	 * @var float $startTime When the timer started.
	 * $var float $pauseTime Used to remove the amount of paused time from the total time.
	 */
	var $startTime;
	var $pauseTime;

	/**
	 * constructor - instantiating an object will automatically start the timer, unless you pass in false
	 *
	 * @param bool $timeout Optional.  Pass false if you do not want the timer to start when the object is created.
	 * 
	 */
	function __construct( bool $start = true ) {
		if( $start ) {
			$this->start();
		}
	}

	/**
	 * getTime - used by all other methods
	 *
	 * @return float Calls microtime() to get current time in microseconds.
	 */
	function getTime() : float {
		return microtime( true );
	}

	/**
	 * start - Reset and start the timer.
	 *
	 * @return void
	 */
	function start() : void {
		$this->startTime = $this->getTime();
		$this->pauseTime = 0;
	}

	/**
	 * pause - Pause the timer.
	 *
	 * @return void
	 */
	function pause() : void {
		$this->pauseTime = $this->getTime();
	}

	/**
	 * unpause - Unpause the timer.  The amount of paused time is excluded from the total time.
	 *
	 * @return void
	 */
	function unpause() : void {
		$this->startTime += ( $this->getTime() - $this->pauseTime );
		$this->pauseTime = 0;
	}

	/**
	 * get - Get the formatted time elapsed.  This does not pause the timer.
	 *
	 * @param int $decimals The of decimal places in the returned time string.
	 *
	 * @return string The time formatted in seconds.
	 */
	function get( int $decimals = 8 ) : string {
		return number_format( ( $this->getTime() - $this->startTime ), $decimals );
	}

}