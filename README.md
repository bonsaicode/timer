# bonsaicode/timer
Timer utility class.

* Timer class for benchmarking your code.

# Requirements
* PHP 7.1 or higher

# Installation
To install the module, run the following command in the console:

`$ composer require "bonsaicode/timer"`

# Usage

	<?php
		# instantiating Timer automatically starts the timer unless you pass false to the constructor
		$timer = new BonsaiCode\Timer();
		
		# [ put code here that you want to time ]

		# get the elapsed time as a string, defaults to 8 decimal places
		echo 'Elapsed seconds: '.$timer->get()."\n";

		# restart the timer
		$timer->start();

		# [ put code here that you want to time ]

		# pause the timer
		$timer->pause();

		# [ code you don't want to time ]

		# resume the timer
		$timer->unpause();

		# show elapsed time since the last start, exclude all paused time
		echo 'Elapsed seconds: '.$timer->get()."\n";
	?>

# Credits
Based on https://davidwalsh.name/php-timer-benchmark